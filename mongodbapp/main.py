import uvicorn
import string
from fastapi import FastAPI
from books_route import router as books_router
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["*"]
 
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(books_router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=5000, reload=True, debug=True, log_level="debug")